package org.bitbucket.jack_basukeraihu.CustomMotd;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.bitbucket.jack_basukeraihu.CustomMotd.util.MessageUtil;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class MotdConfig {

	public static CustomMotd plugin = MotdListener.plugin;

	public static boolean FakeOnlinePlayers;
	public static String MaxPlayer;
	public static String OnlinePlayer;

	public static boolean FakePlayerList;
	public static List<String> PlayerList;

	public static String line1;
	public static String line2;

	static FileConfiguration conf;

	static final private Charset CONFIG_CHAREST=StandardCharsets.UTF_8;

	public static void reloadConfig() {
		plugin.reloadConfig();
		String confFilePath = plugin.getDataFolder() + File.separator + "config.yml";

		try (Reader reader = new InputStreamReader(new FileInputStream(confFilePath), CONFIG_CHAREST)) {
			conf = new YamlConfiguration();
			conf.load(reader);
		} catch (Exception e) {
			System.out.println(e);
		}

		FakeOnlinePlayers = conf.getBoolean("FakeOnlinePlayers");
		MaxPlayer = conf.getString("MaxPlayer");
		OnlinePlayer = conf.getString("OnlinePlayer");

		FakePlayerList = conf.getBoolean("FakePlayerList");
		PlayerList = conf.getStringList("PlayerList");

		line1 = conf.getString("SendMotd.line1");
		line2 = conf.getString("SendMotd.line2");

		line1 = MessageUtil.MotdFormat(line1);
		line2 = MessageUtil.MotdFormat(line2);
	}

}
