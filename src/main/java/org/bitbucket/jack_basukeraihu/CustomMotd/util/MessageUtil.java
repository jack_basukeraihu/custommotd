package org.bitbucket.jack_basukeraihu.CustomMotd.util;

import org.bitbucket.jack_basukeraihu.CustomMotd.MotdConfig;
import org.bukkit.Bukkit;

public class MessageUtil {

	public static String ColorCode(String str) {
		return str.replace("&", "§");
	}

	@SuppressWarnings("deprecation")
	public static String MotdFormat(String str) {
		str = str.replace("%ver", Bukkit.getBukkitVersion());
		if (MotdConfig.FakeOnlinePlayers) {
			str = str.replace("%online", String.valueOf(OnlinePlayersUtil.OnlinePlayers()));
			str = str.replace("%max", String.valueOf(OnlinePlayersUtil.MaxPlayers()));
		} else {
			str = str.replace("%online", String.valueOf(Bukkit.getOnlinePlayers().length));
			str = str.replace("%max", String.valueOf(Bukkit.getMaxPlayers()));
		}

		return ColorCode(str);
	}

}
