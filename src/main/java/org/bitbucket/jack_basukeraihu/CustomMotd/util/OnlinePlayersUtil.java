package org.bitbucket.jack_basukeraihu.CustomMotd.util;

import org.bitbucket.jack_basukeraihu.CustomMotd.MotdConfig;
import org.bukkit.Bukkit;

public class OnlinePlayersUtil {

	@SuppressWarnings("deprecation")
	public static int MaxPlayers() {
		int max = 0;

		if (isInteger(MotdConfig.MaxPlayer)) {
			max = Integer.parseInt(MotdConfig.MaxPlayer);
		} else {
			if (MotdConfig.MaxPlayer.equalsIgnoreCase("players")) {
				max = Bukkit.getOnlinePlayers().length;
			}
			if (MotdConfig.MaxPlayer.equalsIgnoreCase("none")) {
				max = Bukkit.getMaxPlayers();
			}
		}

		return max;
	}

	@SuppressWarnings("deprecation")
	public static int OnlinePlayers() {
		int online = 0;

		if (isInteger(MotdConfig.OnlinePlayer)) {
			online = Integer.parseInt(MotdConfig.OnlinePlayer);
		} else {
			if (MotdConfig.OnlinePlayer.equalsIgnoreCase("rest")) {
				online = (MaxPlayers() - 1);
			}
			if (MotdConfig.OnlinePlayer.equalsIgnoreCase("none")) {
				online = Bukkit.getOnlinePlayers().length;
			}
		}

		return online;
	}

	@SuppressWarnings("unused")
	static boolean isInteger(String num) {
		try {
			int n = Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
