package org.bitbucket.jack_basukeraihu.CustomMotd;

import org.bitbucket.jack_basukeraihu.CustomMotd.SendPacket.setupPacket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomMotd extends JavaPlugin {

	public void onEnable() {
		PluginManager pm =Bukkit.getServer().getPluginManager();
		pm.registerEvents(new MotdListener(this), this);
		if (!Bukkit.getServer().getPluginManager().isPluginEnabled("ProtocolLib")) {
			this.getLogger().severe("ProtocolLibが読み込まれていません プラグインを停止します");
			this.getPluginLoader().disablePlugin(this);
		} else {
			setupPacket.SendCustomMotd();
			saveDefaultConfig();
			MotdConfig.reloadConfig();
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("motd")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.GREEN + "/motd reload :Configファイルを再読み込みします");
				return true;
			}
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("reload")) {
					if ((sender.hasPermission("motd.admin")) || (sender.isOp())) {
						MotdConfig.reloadConfig();
						sender.sendMessage(ChatColor.YELLOW + "Configファイルを再読み込みしました");
					} else {
						sender.sendMessage(ChatColor.RED + "権限がありません");
					}
				}
			}
		}
		return false;
	}

}
