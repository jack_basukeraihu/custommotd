package org.bitbucket.jack_basukeraihu.CustomMotd.SendPacket;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jack_basukeraihu.CustomMotd.MotdConfig;
import org.bitbucket.jack_basukeraihu.CustomMotd.MotdListener;
import org.bitbucket.jack_basukeraihu.CustomMotd.util.MessageUtil;
import org.bitbucket.jack_basukeraihu.CustomMotd.util.OnlinePlayersUtil;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedServerPing;

public class setupPacket {
	static final ProtocolManager manager = ProtocolLibrary.getProtocolManager();

	public static void SendCustomMotd() {
		manager.addPacketListener(new PacketAdapter(PacketAdapter.params(MotdListener.plugin, PacketType.Status.Server.OUT_SERVER_INFO).listenerPriority(ListenerPriority.HIGHEST)) {
			@SuppressWarnings("deprecation")
			@Override
			public void onPacketSending(final PacketEvent e) {
				WrappedServerPing ping = (WrappedServerPing) e.getPacket().getServerPings().read(0);
				String motd = MotdConfig.line1 + "\n" + MotdConfig.line2;
				ping.setMotD(motd);

				if (MotdConfig.FakeOnlinePlayers) {
					ping.setPlayersMaximum(OnlinePlayersUtil.MaxPlayers());
					ping.setPlayersOnline(OnlinePlayersUtil.OnlinePlayers());
				}

				if (MotdConfig.FakePlayerList) {
					List<WrappedGameProfile> list = new ArrayList<WrappedGameProfile>();
					for (String str : MotdConfig.PlayerList) {
						list.add(new WrappedGameProfile("1",MessageUtil.ColorCode(str)));
					}
					ping.setPlayers(list);
				}
			}
		});
	}
}
